import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import './App.css';

import Content from './content/content';

import job from './data';

console.log(job);

class App extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <div>
          <Content job={job} />
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
