import React from 'react';
import { mapObject, values, map, object } from 'underscore'
import time from 'humanize-time'

import {
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import Toggle from 'material-ui/Toggle';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker';
import LinearProgress from 'material-ui/LinearProgress';

import { SegmentLayout, TableLayout } from './common';

/**
 * Helpers
 */

const capitalize = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

/**
 * Special formatting instructions
 */
const DATE_FIELDS = {
  'startdate': true
}

const TIME_SPAN_FIELDS = {
  'workweek': true
}

const BAR_FIELDS = {
  'clientsupport': true,
  'documentation': true,
  'newfeatures': true,
  'maintenance': true,
  'meetings': true
}

const SALARY_FIELDS = {
  'salary': true
}

const TEAM_FIELDS = {
  'teamsize': true
}

const HOURS_FIELDS = {
  'corehours': true
}


/**
 * Format values
 */
const DateValue = (props) => 
  new Date(props.val).toDateString();

const TimeSpanValue = (props) => 
  time(props.val * 1000)

const BarValue = (props) => 
  <LinearProgress mode="determinate" value={props.val}/>

const SalaryValue = (props) =>
  <span>{props.val.amount / 1000}k {props.val.currency} / {props.val.interval} </span>

const TeamValue = (props) =>
  <span>{props.val.min} - {props.val.max} people</span>

const CoreHoursValue = (props) => 
  <span>{props.val.from / 100} until {props.val.to / 100}</span>

/**
 * Render individual Value
 */
const Value = (props) => {

  if ( DATE_FIELDS[props.vkey] ) {
    return <DateValue {...props} />
  }
  if ( TIME_SPAN_FIELDS[props.vkey] ) {
    return <TimeSpanValue {...props} />
  }
  if ( BAR_FIELDS[props.vkey] ) {
    return <BarValue {...props} />
  }
  if ( SALARY_FIELDS[props.vkey] ) {
    return <SalaryValue {...props} />
  }
  if ( TEAM_FIELDS[props.vkey] ) {
    return <TeamValue {...props} />
  }
  if ( HOURS_FIELDS[props.vkey] ) {
    return <CoreHoursValue {...props} />
  }
  if ( typeof(props.val) === 'string' || typeof(props.val) === 'number') {
    return props.val;
  }
  if ( typeof(props.val) === 'boolean' ) {
    return <Toggle style={{position:'absolute', right: 20, width: 40, top:15}} defaultToggled={props.val}/>
  }
  if ( Array.isArray(props.val) ) {
    return map(props.val, val => <div style={{margin:'15px 0px'}}>{capitalize(val)}</div>)
  }
  return '';
}

/**
 * Render list of values
 */
const Values = (props) => values(mapObject(props.values, (val,vkey) => (
  <TableRow key={vkey}>
    <TableRowColumn>{capitalize(vkey)}</TableRowColumn>
    <TableRowColumn style={{textAlign:'right', position:'relative'}}><Value val={val} vkey={vkey}/></TableRowColumn>
  </TableRow>
)))

/**
 * Render individual Segment
 */
const Segment = (props) => {
  if (props.name === 'headline') return <h1>{props.content}</h1>;

  // convert array to object if needed
  var { content } = props;
  if ( Array.isArray(content) ) {
    content = object(content, [''])
  }

  return (
    <SegmentLayout name={capitalize(props.name)}>
      <TableLayout>
        <Values values={content}/>
      </TableLayout>
    </SegmentLayout>
  )
}

/**
 * All content
 */
const Content = (props) => values(mapObject(props.job, (val,key) => (
  <Segment name={key} content={val} key={key} />
)))

export default Content;
