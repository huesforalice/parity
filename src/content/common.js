import React from 'react';
import Paper from 'material-ui/Paper';
import {
  Table,
  TableBody
} from 'material-ui/Table';

const segmentStyle = {
  maxWidth: 500,
  margin: '20px auto',
  color: '#333'
}

export const TitleLayout = (props) => (
  <h1>{props.value}</h1>
)

export const SegmentLayout = (props) => (
  <div style={segmentStyle}>
    <h3>{props.name}</h3>
    <Paper style={{padding:20}} zDepth={1}>
      {props.children}
    </Paper>
  </div>
)

export const TableLayout = (props) => (
  <Table>
    <TableBody>
      {props.children}
    </TableBody>
  </Table>
)