# Prerequesites
* Have npm installed. Alternatively yarn.

# Install

 * Clone Repo
 * Run ```npm install``` or ````yarn````
 * Run ```npm start``` or ````yarn start````
 * Navigate to localhost:3000 with your browser

 
# Frameworks used
 * React
 * Material UI

# Stack I would use for a 'real' web app
 * Typescript (compiles to Javascript)
 * React
 * Redux for state management
 * Something for side effects, I prefer redux-sagas
 * Chai & Jasmine for running tests
 * Have continuous integration set up
 